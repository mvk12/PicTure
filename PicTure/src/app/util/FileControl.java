package app.util;

import java.io.File;

import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class FileControl {
	private FileChooser chooser;
	private Stage _stage;
	private String ext;
	
	
	public FileControl(Stage stage){
		this.chooser = new FileChooser();
		this.set_stage(stage);
		configure();
		this.ext = null;
	}
	
	public Stage get_stage() { return _stage; }

	public void set_stage(Stage _stage) { this._stage = _stage; }
	
	public String getExt() { return ext; }

	public void setExt(String ext) { this.ext = ext; }

	private void configure() {
		this.chooser.setTitle("Open Image");
		this.chooser.getExtensionFilters().addAll(
				new FileChooser.ExtensionFilter("JPG files", "*.jpg","*.jpeg"),
				new FileChooser.ExtensionFilter("PNG files", "*.png"),
				new FileChooser.ExtensionFilter("TIFF files", "*.tif","*.tiff")
				);
	}

	public File getReferenceFile(Stage newStage){
		File _file = null;
		if(newStage!= null)
			this.set_stage(newStage);
		
		_file = this.chooser.showOpenDialog(newStage);
	
		try{
			String a = _file.getName();
			if( a.endsWith("tiff") || a.endsWith("tif") ){
				
			}
				
				
		}catch(Exception ex){
			
		}
		return _file;
	}
}
