package app.util;

import javafx.collections.ObservableList;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;

public class HistogramControl {

	@SafeVarargs
	public static void addSeries(AreaChart<Number, Number> chart, XYChart.Series<Number, Number>... allSeries){
		ObservableList<Series<Number, Number>> listSerie = chart.getData();
		listSerie.remove(0, listSerie.size());
		
		for (XYChart.Series<Number, Number> serie : allSeries) 
			listSerie.add(serie);
	}
}
