package app.model;

import java.io.File;
import java.util.Random;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class ImageOperations {
	private static final int GRAY_LEVELS = 256;
	
	/* Create Image*/
	public static Image createImage(File file, boolean smooth){
		return new Image (file.toURI().toString(), 512, 512, false, smooth);
	}
	
	/* Example Method
	 * 
	 * public static Image name(Image image, Params...){	
		
		double M = image.getWidth();
		double N = image.getHeight();
		
		PixelReader reader = image.getPixelReader();
		
		WritableImage wImage = new WritableImage((int)M, (int)N);
		PixelWriter writer = wImage.getPixelWriter();
		
		Color color; //, colorF;
		for (int i = 0; i < (int)M; i++) {
			for (int j = 0; j < (int)N; j++) {
				//Operations on wImage
			}
		}
		
		return wImage;
	}
	 * 
	 * */
	
	
	/* Binary Method*/
	/*public static Image binaryImage(Image image, int div){
		
		return null;
	}*/
	
	/* Negative Image Method
	 * 
	 * */
	
	/* Dispose/Compress Image Method 
	 * 
	 * */
	
	/* Power Function Method*/
	
	/* PieceWise Contrast Method*/

	/* Slicing */
	
	/* Bit Plane Slicing*/
	/* Gray Scale Methods*/
	public static Image grayScale(File file){
		Image image = new Image (file.toURI().toString(), 512, 512, false, true);
		return grayScale(image);
	}

	public static Image grayScale(Image image){
		 
		PixelReader pixelReader = image.getPixelReader();
		
		System.out.println("");
		WritableImage wImage = new WritableImage( (int)image.getWidth(),(int)image.getHeight() );
        PixelWriter pixelWriter = wImage.getPixelWriter();
	        
        double R, G, B;
        Color color;
        
        for(int readY = 0;readY < image.getHeight();readY++){
            for (int readX = 0;readX < image.getWidth();readX++){ 
            	color = pixelReader.getColor(readX, readY);
                R=255*color.getRed();
                G=255*color.getGreen();
                B=255*color.getBlue();
                
                int GR = grayOne(R, G, B);
                pixelWriter.setColor(readX, readY,Color.rgb(GR,GR,GR));
            }
        }
    	return wImage;
    }
	
	protected static int grayOne(double R, double G, double B) {
		return (int)((R+G+B)/3);
	}
	
	protected static int grayTwo(double R, double G, double B) {
		return (int)(0.2126*R + 0.7152*G + 0.0722*B);
	}
	
	protected static int grayThree(double R, double G, double B) {
		return (int) (0.299*R + 0.587*G + 0.114*B);
	}
	
	/* Ecualize Methods*/
	public static Image ecualize(Image image){
		
		ImageStadistics pControl = new ImageStadistics(image);
		pControl.calculeGrayPixels();
		
		double M = image.getWidth();
		double N = image.getHeight();
		
		double vConst = (GRAY_LEVELS - 1)/(M*N);
		
		int[] EQArray = equalizeArray(pControl.getRed());
		
		PixelReader reader = image.getPixelReader();
		
		WritableImage wImage = new WritableImage((int)M, (int)N);
		PixelWriter writer = wImage.getPixelWriter();
		
		Color color, colorF;
		for (int i = 0; i < (int)M; i++) {
			for (int j = 0; j < (int)N; j++) {
				color = reader.getColor(i, j);
				int grayColor = (int)(255 * color.getRed());
				int newColor = (int) (EQArray[grayColor] * vConst);
				//int newColor = (int) (Math.floor(EQArray[grayColor] * vConst));
				colorF = Color.rgb(newColor, newColor, newColor);
				writer.setColor(i, j, colorF);
			}
		}
		
		return wImage;
	}
	
	private static int[] equalizeArray(int[] array){
		for(int i=1; i<array.length; i++)
			array[i] += array[i-1];
		
		return array;
	}

	/*Adaptative EQ Method*/
	public static Image adapEqualize(File file, int mask){
		return adapEqualize( new Image(file.toURI().toString(), 512, 512, false, true), mask);
	}
	
	public static Image adapEqualize(Image image, int mask){
		int addRC = mask - 1;
		int offset = addRC / 2;
		
		PixelReader reader = image.getPixelReader();
		
		
		int[][] map = new int[(int)(image.getWidth() + mask - 1)][(int)(image.getHeight() + mask - 1)];
		
		double M = image.getWidth();
		double N = image.getHeight();
		
		WritableImage wImage = new WritableImage((int)M, (int)N);
		PixelWriter writer = wImage.getPixelWriter();
				
		Color colorImage;
		
		for (int i = 0; i < (int)M; i++) {
			for (int j = 0; j < (int)N; j++) {
				colorImage = reader.getColor(i, j);
				map[i+offset][j+offset] = (int)(255 * colorImage.getRed());
			}
		}
		int center = 0, counter = 0, nValue;
		int area = mask * mask;
		double pCount = 0.0;
		
		for (int i = offset; i < ((int)M)+offset; i++) {
			for (int j = offset; j < ((int)N)+offset; j++) {
				center = map[i][j];
				for (int k = i-offset; k <= i+offset; k++) {
					for (int l = j-offset; l <= j+offset; l++) {
						if( map[k][l] <= center)
							++counter;
					}
				}
				
				pCount = (double)counter/(double)area;
				nValue = (int) (pCount * 255);
				
				map[i][j] = nValue;				
				
				counter = 0;
			}
		}
		
		for (int i = 0; i < (int)M ; i++) {
			for (int j = 0; j < (int)N ; j++) {
				nValue = map[i+offset][j+offset];
				writer.setColor(i, j, Color.rgb(nValue, nValue, nValue));
			}
		}
		
		return wImage;
	}
	/* Compare Method*/
	public static Image compareImages(Image originalImage, Image temperedImage, int offsetX, int offsetY, int range) {
		/*Calcule Dimensions*/
		int M = (int)originalImage.getWidth();
		int N = (int)originalImage.getHeight();
		
		/*Calcule Offsets*/
		int d1x = 0, d1y = 0, d2x = 0, d2y = 0;
		if(offsetX < 0){ d1x = Math.abs(offsetX); d2x = 0; }
		if(offsetX > 0){ d2x = Math.abs(offsetX); d1x = 0; }
		if(offsetY < 0){ d1y = Math.abs(offsetY); d2y = 0; }
		if(offsetY > 0){ d2y = Math.abs(offsetY); d1y = 0; }
		
		/*Readers and Writers*/
		PixelReader readerOriginal = originalImage.getPixelReader();
		PixelReader readerTempered = temperedImage.getPixelReader();
		WritableImage wImage = new WritableImage(M, N);
		PixelWriter writer = wImage.getPixelWriter();
		
		Color colorOriginal, colorTempered, wcolor;
		double color1, color2;
		
		/* Iterate*/
		for (int i = 0; i < M; i++) {
			for (int j = 0; j < N; j++) {
				try{
					
					colorOriginal = readerOriginal.getColor(i+d1x, j+d1y);
					colorTempered = readerTempered.getColor(i+d2x, j+d2y);
					wcolor = Color.BLACK;
					
					color1 = 255*colorOriginal.getRed();
					color2 = 255*colorTempered.getRed();
					
					if(Math.abs(color1 - color2) >= range)
						wcolor = Color.WHITE;
					
					color1 = 255*colorOriginal.getGreen();
					color2 = 255*colorTempered.getGreen();
					
					if(Math.abs(color1 - color2) >= range)
						wcolor = Color.WHITE;
					
					color1 = 255*colorOriginal.getBlue();
					color2 = 255*colorTempered.getBlue();
					if(Math.abs(color1 - color2) >= range)
						wcolor = Color.WHITE;
					
					writer.setColor(i, j, wcolor);
					
				}catch(IndexOutOfBoundsException iobe){
					break;
				}
			}
		}
		return wImage;
	}
	
	/* Gauss Filter*/
  
    private static double[][] generaFiltro (double theta , int mask)
    {
        double [][] filtro =new double [mask][mask];
        for (int i = 0; i <mask ; i++) {
            for (int j = 0; j <mask; j++) {
              //  filtro[i][j]= Math.pow(Math.E,-(i*i+j*j/(2*theta*theta)) );

                filtro[i][j] = ( (1/(2*Math.PI*theta*theta))* Math.pow(Math.E,-(i*i+j*j/(2*theta*theta)) ));
            }
        }
        double suma =0;
        for (int i = 0; i < mask; i++) {
            for (int j = 0; j <mask ; j++) {
                suma =suma +filtro[i][j];
            }
        }
        for (int i = 0; i < mask; i++) {
            for (int j = 0; j <mask ; j++) {
                filtro[i][j]=(filtro[i][j]/suma) ;
            }
        }
        
        
        return filtro;
    }
    public  static Image gauss(Image image,int mask, double theta  )
    {
    
                int addRC = mask - 1;
            int offset = addRC / 2;
            
            PixelReader reader = image.getPixelReader();
            
            
            double M = image.getWidth();
            double N = image.getHeight();
            int[][] map = new int[(int)(M + addRC)][(int)(N + addRC)];
            int[][] map_filtro = new int[(int)M][(int)N];
            double[][] filtro = new double [mask][mask];
            
            WritableImage wImage = new WritableImage((int)M, (int)N);
            PixelWriter writer = wImage.getPixelWriter();
                            
            Color colorImage;
            filtro = generaFiltro(theta, mask);
            int centro = (int)((mask/2)-.5);
             
            for (int i = 0; i < (int)M; i++) {
                    for (int j = 0; j < (int)N; j++) {
                            colorImage = reader.getColor(i, j);
                            map[i+offset][j+offset] = (int)(255 * colorImage.getRed());
                    }
            }
            
            for (int i = offset; i < ((int)M)+offset; i++) {
                for (int j = offset; j < ((int)M)+offset; j++) {
                    
                    map_filtro[i-offset][j-offset]=(int)(map[i][j]*filtro[centro][centro]);
                }
            
            }
            int nValue=0;
            for (int i = 0; i < (int)M ; i++) {
                    for (int j = 0; j < (int)N ; j++) {
                            nValue=map_filtro[i][j];
                            writer.setColor(i, j, Color.rgb(nValue, nValue, nValue));
                    }
            }
            
            return ecualize(wImage);
    }

	/* Noise Methods*/
    public static Image anadirRuidoAleatorio(Image image){
        /**/
        PixelReader reader = image.getPixelReader();
        int Width = (int)image.getWidth();
        int Height = (int) image.getHeight();
        WritableImage wimage = new WritableImage(Width, Height);
        PixelWriter writer = wimage.getPixelWriter();
        
        Random rand = new Random();
        int low = -30;
        
        Color color = Color.BLACK;
        /**/
        for(int i=0; i<Width; i++){
            for(int j = 0; j< Height ; j++){
                color = reader.getColor(i, j);
                int c = (int)(255 * color.getRed());
                
                c = c + low + rand.nextInt(61);
                
                if(c > 255)
                    c = 255;
                if(c < 0)
                    c = 0;
                
                double cc = (double) c / 255.0 ;
                Color newColor = new Color(cc, cc, cc, 1);
                writer.setColor(i, j, newColor);
            }
        }
        
        return wimage;
    }
    
    public static Image anadirRuidoGaussiano(Image image, float mean, float variance){
        /**/
        PixelReader reader = image.getPixelReader();
        int Width = (int)image.getWidth();
        int Height = (int) image.getHeight();
        
        WritableImage wimage = new WritableImage(Width, Height);
        PixelWriter writer = wimage.getPixelWriter();
        Random rand = new Random();
        
        Double x, xn, x1, x2, tmp;
        Color color = Color.BLACK;
        
        for (int i=0; i<Width; i++) {
            for (int j=0; j<Height; j++) {
                // x1, x2: uniform random variables in the range [0, 1]
                do 
                    x1 = rand.nextDouble();
                while (x1 == 0 || x1>1);    // x1 can't be zero
                do
                    x2 = rand.nextDouble();
                while(x2==0 || x2 > 1);
                
                // x, y: unit normal random variables, ~N(0, 1)
                // xn, yn: normal random variables, ~N(mean, variance)
                x = Math.sqrt(-2*Math.log(x1)) * Math.cos(2*Math.PI*x2);
                xn = mean + Math.sqrt(variance) * x;
                
                // y = Math.sqrt(-2*Math.log(x1)) * Math.sin(2*PI*x2);
                // yn = mean + Math.sqrt(variance) * y;
                
                color = reader.getColor(i, j);
                int c = (int)color.getRed()*255;
                tmp = c + xn;   // Add noise to pixel
                if (tmp < 0)
                    c = 0;
                else if (tmp > 255)
                    c = 255;
                else
                    c = tmp.intValue();
                
                double cc = (double) c / 255.0 ;
                Color newColor = new Color(cc, cc, cc, 1);
                
                writer.setColor(i, j, newColor);
            }
        }
        return wimage;
    }

}
