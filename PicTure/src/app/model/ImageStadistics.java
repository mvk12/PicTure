package app.model;

import javafx.collections.ObservableList;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;

public class ImageStadistics {

	private Image image;
	private int[] redValue;
	private int[] greenValue;
	private int[] blueValue;
	//private int[] alphaValue;
	
	public ImageStadistics(Image image){
		this.image = image;
		this.redValue = new int[256];
		this.greenValue = new int[256];
		this.blueValue = new int[256];
		//this.alphaValue = new int[256];
	}
	
	public int[] getRed(){ return redValue; }
	
	public int[] getGreen(){ return greenValue; }
	
	public int[] getBlue(){ return blueValue; }
	
	//public int[] getAlpha(){ return alphaValue; }
	
	public void calculePixels() {
		PixelReader reader = this.image.getPixelReader();
		int width = (int)this.image.getWidth();
		int height = (int)this.image.getHeight();
		Color color;
		int value;
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				
				color = reader.getColor(i, j);
				
				value = (int)(255*color.getRed());
				this.redValue[value]++;
				
				value = (int)(255*color.getGreen());
				this.greenValue[value]++;
				
				value = (int)(255*color.getBlue());
				this.blueValue[value]++;
				
			}
		}
	}
	
	public void calculeGrayPixels() {
		PixelReader reader = this.image.getPixelReader();
		int width = (int)this.image.getWidth();
		int height = (int)this.image.getHeight();
		Color color;
		int value;
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				
				color = reader.getColor(i, j);
				
				value = (int)(255*color.getRed());
				this.redValue[value]++;
								
			}
		}
		
		this.greenValue = redValue;
		this.blueValue = redValue;
	}
	
	public static XYChart.Series<Number, Number> getSerie(int[] array, String name){
		XYChart.Series<Number, Number> nuevaSerie = new XYChart.Series<>();
		nuevaSerie.setName(name);
		
		ObservableList<Data<Number, Number>> _list = nuevaSerie.getData();
		
		for(int i = 0; i<array.length; i++)
			_list.add(new XYChart.Data<>(i,array[i]));
		
		return nuevaSerie;
	}
}
