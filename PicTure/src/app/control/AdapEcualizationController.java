package app.control;

import java.io.File;

import app.Main;
import app.model.ImageOperations;
import app.model.ImageStadistics;
import app.util.FileControl;
import app.util.HistogramControl;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public class AdapEcualizationController {

	private Main appMain;
	private FileControl controlChooser;
	
	@FXML private Button open;
	@FXML private Button restoreImage;
	@FXML private Button maskPlus;
	@FXML private Button maskMinus;
	@FXML private TextField maskValue;
	@FXML private Button eqImage;
	@FXML private Label nameImage;
	@FXML private ImageView imgView;
	@FXML private Pane _work;
	@FXML private AreaChart<Number, Number> chart;
	
	private static int mask = 3;
	private static Image imgStatic = null;
	public AdapEcualizationController(){
		;
	}

	@FXML private void initialize(){
		NumberAxis xAxis = new NumberAxis();
		NumberAxis yAxis = new NumberAxis();
		
		chart = new AreaChart<>(xAxis, yAxis);
		chart.setPrefWidth(1400);
		chart.setPrefHeight(500);
		chart.setAnimated(true);
		
		ObservableList<Node> _list = _work.getChildren();
		_list.remove(0, _list.size());
		_list.add(chart);
		
		maskValue.setText(""+mask);
		restoreImage.setDisable(true);
		imgStatic = imgView.getImage();
	}
	
	public Main getAppMain() {
		return appMain;
	}

	public void setAppMain(Main appMain) {
		this.appMain = appMain;
		this.controlChooser = new FileControl(appMain.getStage());
	}
	
	@FXML private void handleLoad(){
		File _file = this.controlChooser.getReferenceFile(null);
		try {
			nameImage.setText(_file.getName());
			imgStatic = ImageOperations.grayScale(_file);
			imgView.setImage(imgStatic);
			restoreImage.setDisable(true);
		} catch (NullPointerException npe) {
			nameImage.setText("No se eligi� Imagen");
		}
	}
	
	@FXML private void handleRestoreToOriginalImage(){
		imgView.setImage(imgStatic);
		restoreImage.setDisable(true);
	}
	
	@FXML private void handleEQImage(){
		Image image = imgView.getImage();
		Alert alerta = new Alert(Alert.AlertType.INFORMATION);
		alerta.setTitle("Procesando...");
		alerta.setHeaderText("Por Favor Espere...");
		alerta.setContentText("Pulse Aceptar cuando la aplicaci�n se lo permita");
		alerta.show();
		
		long timeActual = System.currentTimeMillis();
		Image imgResult = ImageOperations.adapEqualize(image, mask);
		long timeEnd = System.currentTimeMillis();
		this.appMain.sendToConsole("Tiempo de Procesado: "+(timeEnd-timeActual)+" milisegundos");
		imgView.setImage(imgResult);
		
		ImageStadistics pControl =  new ImageStadistics(imgResult);
		pControl.calculeGrayPixels();
		
		HistogramControl.addSeries(chart,
				ImageStadistics.getSerie(pControl.getRed(), "Ecualizado Adaptat_Ivo"));
		
		restoreImage.setDisable(false);
	}
	
	@FXML private void handleMaskMinus(){
		if(mask > 3){ mask -= 2; maskValue.setText(""+mask);}
	}
	
	@FXML private void handleMaskPlus(){
		if(mask < 155){ mask += 2; maskValue.setText(""+mask);}
	}
}
