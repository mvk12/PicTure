package app.control;

import java.io.File;

import app.Main;
import app.model.ImageOperations;
import app.model.ImageStadistics;
import app.util.FileControl;
import app.util.HistogramControl;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public class EcualizationController {

	private Main appMain;
	private FileControl controlChooser;
	
	@FXML Button load;
	@FXML Button eqImage;
	@FXML Label nameImage;	
	@FXML ImageView viewOriginal;
	@FXML ImageView viewResult;
	@FXML Pane _work1;
	@FXML Pane _work2;
	@FXML AreaChart<Number, Number> chartOriginal;
	@FXML AreaChart<Number, Number> chartEqualize;
	
	public EcualizationController(){
		;
	}

	@FXML private void initialize(){
		eqImage.setDisable(true);
		NumberAxis xAxis = new NumberAxis();
		NumberAxis yAxis = new NumberAxis();
		
		chartOriginal = new AreaChart<>(xAxis, yAxis);
		chartOriginal.setPrefWidth(1400);
		chartOriginal.setPrefHeight(500);
		chartOriginal.setAnimated(true);
		
		ObservableList<Node> _list = _work1.getChildren();
		_list.remove(0, _list.size());
		_list.add(chartOriginal);
		
		xAxis = new NumberAxis();
		yAxis = new NumberAxis();
		
		chartEqualize = new AreaChart<>(xAxis, yAxis);
		chartEqualize.setPrefWidth(1400);
		chartEqualize.setPrefHeight(500);
		chartEqualize.setAnimated(true);
		
		_list = _work2.getChildren();
		_list.remove(0, _list.size());
		_list.add(chartEqualize);
		
	}
	
	public Main getAppMain() {
		return appMain;
	}

	public void setAppMain(Main appMain) {
		this.appMain = appMain;
		controlChooser = new FileControl(appMain.getStage());
	}
	
	@FXML
	private void handleLoad(){
		File _file = this.controlChooser.getReferenceFile(null);
		try {
			//String dir = _file.toURI().toString();
			nameImage.setText(_file.getName());
			Image img_1 = ImageOperations.grayScale(_file);
			
			viewOriginal.setImage(img_1);
			ImageStadistics pControl = new ImageStadistics(img_1);
			pControl.calculeGrayPixels();
			
			HistogramControl.addSeries(chartOriginal, 
					ImageStadistics.getSerie(pControl.getRed(), "Escala de Grises"));
			
			eqImage.setDisable(false);
		} catch (NullPointerException npe) {
			nameImage.setText("No se eligi� Imagen");
		}
	}
	
	@FXML
	private void handleEQImage(){
		Image EQImage = viewOriginal.getImage();
		EQImage = ImageOperations.ecualize(EQImage);
		viewResult.setImage(EQImage);
		
		ImageStadistics pControl = new ImageStadistics(EQImage);
		pControl.calculeGrayPixels();
		
		HistogramControl.addSeries(chartEqualize,
				ImageStadistics.getSerie(pControl.getRed(), "Escala de Grises Ecualizada"));
	}
}
