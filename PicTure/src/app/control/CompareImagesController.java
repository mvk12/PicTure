package app.control;

import java.io.File;

import app.Main;
import app.model.ImageOperations;
import app.util.FileControl;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class CompareImagesController {
	
	private Main appMain;
	private FileControl controlChooser;
	
	@FXML private Button load1;
	@FXML private Button load2;
	@FXML private Button compare;
	@FXML private Button offsetXPlus;
	@FXML private Button offsetXMinus;
	@FXML private Button offsetYPlus;
	@FXML private Button offsetYMinus;
	@FXML private Button rangePlus;
	@FXML private Button rangeMinus;
	@FXML private TextField offSetX;
	@FXML private TextField offSetY;
	@FXML private TextField range;
	@FXML private ImageView img_view1;
	@FXML private ImageView img_view2;
	@FXML private Label label1;
	@FXML private Label label2;
	
	/**/
	private Image img_1, img_2;
	private static int countX = 0, countY = 0, rango = 1;
	
	public CompareImagesController(){
		;
	}
	
	@FXML protected void initialize(){
		offSetX.setText(Integer.toString(countX));
		offSetY.setText(Integer.toString(countY));
		range.setText(Integer.toString(rango));
	}
	public Main getAppMain() {
		return appMain;
	}

	public void setAppMain(Main appMain) {
		this.appMain = appMain;
		this.controlChooser = new FileControl(appMain.getStage());
	}
	
	@FXML
	private void handleLoad_1(){
		File _file = this.controlChooser.getReferenceFile(null);
		try {
			//String dir = _file.toURI().toString();
			label1.setText(_file.getName());
			//img_1 = new Image(dir);
			img_1 = ImageOperations.grayScale(_file);
			img_view1.setImage(img_1);
			
		} catch (NullPointerException npe) {
			label1.setText("No se eligi� Imagen");
		}
	}
	
	@FXML
	private void handleLoad_2(){
		File _file = this.controlChooser.getReferenceFile(null);
		try {
			//String dir = _file.toURI().toString();
			label2.setText(_file.getName());
			//img_2 = new Image(dir);
			img_2 = ImageOperations.grayScale(_file);
			img_view2.setImage(img_2);
			
		} catch (NullPointerException npe) {
			label2.setText("No se eligi� Imagen");
		}
	}
	
	@FXML
	private void handleDoCompare(){
		/*Do compare here*/
		Image result = ImageOperations.compareImages(img_view1.getImage(), img_view2.getImage(),countX, countY, rango);
		this.appMain.outImageView(result, countX, countY, rango);
	}
	
	@FXML private void handleXPlus(){++countX; offSetX.setText(""+countX);}
	
	@FXML private void handleXMinus(){--countX; offSetX.setText(""+countX);}
	
	@FXML private void handleYPlus(){++countY; offSetY.setText(""+countY);}
	
	@FXML private void handleYMinus(){--countY; offSetY.setText(""+countY);}
	
	@FXML private void handleRangePlus(){++rango; range.setText(""+rango);}
	
	@FXML private void handleRangeMinus(){ if(rango > 1) {--rango; range.setText(""+rango);} }
}
