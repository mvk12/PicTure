package app.control;

import java.io.File;

import app.Main;
import app.model.ImageStadistics;
import app.util.FileControl;
import app.util.HistogramControl;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public class HistogramController {
	
	private Main appMain;
	private FileControl controlChooser; 
		
	@FXML private Button load;
	@FXML private Button save;
	@FXML private Label direction;
	@FXML private ImageView imgView;
	@FXML private Pane _work;
	@FXML private AreaChart<Number, Number> chart;
	
	public HistogramController(){
		;
	}
	
	@FXML private void initialize(){
		NumberAxis xAxis = new NumberAxis();
		NumberAxis yAxis = new NumberAxis();
		
		chart = new AreaChart<>(xAxis, yAxis);
		chart.setPrefWidth(1400);
		chart.setPrefHeight(500);
		chart.setAnimated(true);
		
		ObservableList<Node> _list = _work.getChildren();
		_list.remove(0, _list.size());
		_list.add(chart);
	}
	
	public Main getAppMain() {
		return appMain;
	}

	public void setAppMain(Main appMain) {
		this.appMain = appMain;
		this.controlChooser = new FileControl(appMain.getStage());
	}
	
	@FXML
	private void handleChooser(){
		File _file = this.controlChooser.getReferenceFile(null);
		try{
			Image img = new Image(_file.toURI().toString());//, 512, 512, false, true);
			direction.setText(_file.getName());
			imgView.setImage(img);
			
			ImageStadistics pControl = new ImageStadistics(img);
			appMain.sendToConsole("Calculando Histograma");
			pControl.calculePixels();
			appMain.sendToConsole("Desplegando");
			
			HistogramControl.addSeries(chart, 
					ImageStadistics.getSerie(pControl.getRed(), "Serie Roja"),
					ImageStadistics.getSerie(pControl.getGreen(), "Serie Verde"),
					ImageStadistics.getSerie(pControl.getBlue(), "Serie Azul") );

			
		}catch(NullPointerException npe){
			;
		}
	}
}
