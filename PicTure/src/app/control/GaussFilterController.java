package app.control;

import java.io.File;

import app.Main;
import app.model.ImageOperations;
import app.model.ImageStadistics;
import app.util.FileControl;
import app.util.HistogramControl;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public class GaussFilterController {
	private Main appMain;
	private FileControl controlChooser;
	
	@FXML private Button open;
	@FXML private Button restoreImage;
	@FXML private Button maskPlus;
	@FXML private Button maskMinus;
	@FXML private TextField maskTxt;
	@FXML private TextField thetaTxt;
	@FXML private Button gssFilterImage;
	@FXML private Label nameImage;
	@FXML private ImageView imgView;
	@FXML private Pane _work;
	@FXML private AreaChart<Number, Number> chart;
	
	private static int mask = 3;
	private static double theta = 0.5;
	private static Image imgStatic = null;
	public GaussFilterController() {
		;
	}

	@FXML private void initialize(){
		NumberAxis xAxis = new NumberAxis();
		NumberAxis yAxis = new NumberAxis();
		
		chart = new AreaChart<>(xAxis, yAxis);
		chart.setPrefWidth(1400);
		chart.setPrefHeight(500);
		chart.setAnimated(true);
		
		ObservableList<Node> _list = _work.getChildren();
		_list.remove(0, _list.size());
		_list.add(chart);
		
		maskTxt.setText(""+mask);
		thetaTxt.setText(""+theta);
		restoreImage.setDisable(true);
		imgStatic = imgView.getImage();
	}
	
	public Main getAppMain() {
		return appMain;
	}

	public void setAppMain(Main appMain) {
		this.appMain = appMain;
		this.controlChooser = new FileControl(appMain.getStage());
	}
	
	@FXML private void handleLoad(){
		File _file = this.controlChooser.getReferenceFile(null);
		try {
			nameImage.setText(_file.getName());
			imgStatic = ImageOperations.grayScale(_file);
			imgView.setImage(imgStatic);
			restoreImage.setDisable(true);
		} catch (NullPointerException npe) {
			nameImage.setText("No se eligi� Imagen");
		}
	}
	
	@FXML private void handleRestoreToOriginalImage(){
		imgView.setImage(imgStatic);
		restoreImage.setDisable(true);
	}
	
	@FXML private void handleFilter(){
		Image image = imgView.getImage();
		
		/*Aplicar Resultados*/
		Image imgResult = ImageOperations.gauss(image, mask, theta);
		
		imgView.setImage(imgResult);
		
		ImageStadistics pControl =  new ImageStadistics(imgResult);
		pControl.calculeGrayPixels();
		
		HistogramControl.addSeries(chart,
				ImageStadistics.getSerie(pControl.getRed(), "Filtro Gaussiano"));
		
		restoreImage.setDisable(false);
	}
	
	@FXML private void handleMaskMinus(){
		if(mask > 3){ mask -= 2; maskTxt.setText(""+mask);}
	}
	
	@FXML private void handleMaskPlus(){
		if(mask < 155){ mask += 2; maskTxt.setText(""+mask);}
	}
	
	@FXML private void handleThetaValue(){
		try{
			double gssTheta = Double.parseDouble(thetaTxt.getText());
			if(!(gssTheta > 0.0 && gssTheta <= 1.0))
				throw new Exception();
			theta = gssTheta;
		}catch(NumberFormatException nfe){
			Alert alerta = new Alert(Alert.AlertType.ERROR);
			alerta.setHeaderText(thetaTxt.getText()+" no es un n�mero");
			alerta.setContentText("Escriba un valor v�lido mayor a 0.0 o menor e igual a 1.0");
			alerta.showAndWait();
			thetaTxt.setText("");
			theta = 0.5;
			thetaTxt.setText(""+theta);
		}catch (Exception e) {
			Alert alerta = new Alert(Alert.AlertType.ERROR);
			alerta.setHeaderText("Formato �nvalido");
			alerta.setContentText("Aseg�rese de ingresar un numero mayor que 0.0 o menor e igual que 1.0");
			alerta.showAndWait();
			thetaTxt.setText("");
			theta = 0.5;
			thetaTxt.setText(""+theta);
		}
	}
}
