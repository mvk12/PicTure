package app.control;

import app.Main;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class OutImageController {

	private Main appMain;
	private Stage dStage;
	
	@FXML private ImageView imgView;
	@FXML private Button save;
	@FXML private TextField nameAs;
	@FXML private Label LabelX;
	@FXML private Label LabelY;
	@FXML private Label LabelTol;
	
	public OutImageController(){
		;
	}
	
	@FXML protected void initialize(){
		save.setDisable(true);
	}
	@FXML private void handleSave(){
		this.appMain.sendToConsole("Saves As...");
		nameAs.setText("Saving...");
	}
	
	public void setImage(Image image){
		imgView.setImage(image);
	}
	
	public Main getAppMain() {
		return appMain;
	}

	public void setAppMain(Main appMain) {
		this.appMain = appMain;
	}

	public Stage getdStage() {
		return dStage;
	}

	public void setdStage(Stage dStage) {
		this.dStage = dStage;
	}

	public void setLabels(int offX, int offY, int tol) {
		LabelX.setText("Desplazamiento en X: "+offX);
		LabelY.setText("Desplazamiento en Y: "+offY);
		LabelTol.setText("Tolerancia: "+tol);
	}
	
	public void noLabels(){
		LabelX.setVisible(false);
		LabelY.setVisible(false);
		LabelTol.setVisible(false);
	}
	
}
