package app.control;

import java.io.File;

import app.Main;
import app.model.ImageOperations;
import app.model.ImageStadistics;
import app.util.FileControl;
import app.util.HistogramControl;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public class GrayScaleController {

	private Main appMain;
	private FileControl controlChooser;
	
	@FXML private Button load;
	@FXML private Button doGray;
	@FXML private Label nameImage;
	@FXML private ImageView imgView;
	@FXML private Pane _work;
	@FXML private AreaChart<Number, Number> chart;
	
	public GrayScaleController(){
		;
	}

	@FXML private void initialize(){
		NumberAxis xAxis = new NumberAxis();
		NumberAxis yAxis = new NumberAxis();
		
		chart = new AreaChart<>(xAxis, yAxis);
		chart.setPrefWidth(1300);
		chart.setAnimated(true);
		
		ObservableList<Node> _list = _work.getChildren();
		_list.remove(0, _list.size());
		_list.add(chart);
		doGray.setDisable(true);
	}
	
	public Main getAppMain() {
		return appMain;
	}

	public void setAppMain(Main appMain) {
		this.appMain = appMain;
		this.controlChooser = new FileControl(appMain.getStage());
	}
	
	@FXML
	private void handleLoad(){
		File _file = this.controlChooser.getReferenceFile(null);
		try {
			nameImage.setText(_file.getName());
			imgView.setImage(new Image(_file.toURI().toString(), 512, 512, false, true));
			doGray.setDisable(false);
		} catch (NullPointerException npe) {
			Alert alerta = new Alert(Alert.AlertType.ERROR);
			alerta.setHeaderText("Error al procesar");
			alerta.setContentText("El archivo o imagen o es v�lida");
			alerta.showAndWait();
		}
	}
	
	@FXML
	private void handleGrayScale(){
		Image imgPro = ImageOperations.grayScale(imgView.getImage());
		imgView.setImage(imgPro);
		
		ImageStadistics pControl = new ImageStadistics(imgPro);
		pControl.calculeGrayPixels();
		
		HistogramControl.addSeries(chart, ImageStadistics.getSerie(pControl.getRed(), "Escala de Grises"));
	}
}
