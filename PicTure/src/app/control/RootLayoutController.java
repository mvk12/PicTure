package app.control;

import java.util.Optional;

import app.Main;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;

public class RootLayoutController {
	
	private Main appMain;
	
	@FXML private TextField console;
	// Items for Archive Menu
	@FXML private MenuItem open;
	@FXML private MenuItem close;
	// Menu Metodos
	@FXML private MenuItem original;
	@FXML private MenuItem compareImages;
	// Sub menu Realce
	@FXML private MenuItem grayScale;
	@FXML private MenuItem ecualize;
	@FXML private MenuItem ecualizeAdp;
	// Sub menu Filtros
	@FXML private MenuItem average;
	@FXML private MenuItem pass;
	@FXML private MenuItem gauss;
	// Menu Anadir Ruido
	@FXML private MenuItem random;
	@FXML private MenuItem gaussR;
	@FXML private MenuItem bipolar;
	@FXML private MenuItem uniform;
	
	public RootLayoutController(){
		;
	}

	public Main getAppMain() {
		return appMain;
	}
	
	public void setAppMain(Main appMain) {
		this.appMain = appMain;
	}

	public void sendMsg(String msg) {
		this.console.setText(msg);		
	}
	
	@FXML private void closeWindow(){
		try {
			Alert close = new Alert(Alert.AlertType.CONFIRMATION);
			close.setContentText("�Desea Cerrar la Aplicaci�n?");
			
			Optional<ButtonType> option = close.showAndWait();
			
			String result = option.get().getButtonData().toString();
			
			if(result.equals("OK_DONE"))
				Platform.exit();
			
		} catch (Exception e) {
			console.setText("Can�t Stop App");
		}
	}
	
	@FXML private void handleOrigHisto(){
		console.setText("Cambiando Vista: Histograma");
		this.appMain.histogramView();
	}
	
	@FXML private void handleGrayScale(){
		console.setText("Cambiando Vista: Escala de Grises");
		this.appMain.grayScaleView();
	}
	
	@FXML private void handleEcualize(){
		console.setText("Cambiando Vista: Ecualizaci�n");
		this.appMain.ecualizeView();
	}
	
	@FXML private void handleAdapEcualize(){
		console.setText("Cambiando Vista: Ecualizaci�n Adaptativa");
		this.appMain.adapEcualizeView();
	}
	
	@FXML private void handleCompareImages(){
		console.setText("Cambiando Vista: Comparar Im�genes");
		this.appMain.compareImagesView();
	}
	
	@FXML private void handleAverageFilter(){
		console.setText("Cambiando Vista: Filtro de Promedio");
		this.appMain.averageFilterView();
	}
	
	@FXML private void handlePassFilter(){
		console.setText("Cambiando Vista: Filtros Pasa Bajos y Pasa Altos");
		this.appMain.passFiltersView();
	}
	
	@FXML private void handleGaussFilter(){
		console.setText("Cambiando Vista: Filtro Gaussiano");
		this.appMain.gaussFilterView();
	}
	
	@FXML private void handleRandomNoise(){
		console.setText("Cambiando Vista: A�adir Ruido Aleatorio");
		this.appMain.ramdomNoiseView();
	}
	
	@FXML private void handleGaussNoise(){
		console.setText("Cambiando Vista: A�adir Ruido Gaussiano");
		this.appMain.gaussNoiseView();
	}
	
	@FXML private void handleBipolarNoise(){
		console.setText("Cambiando Vista: A�adir Ruido Bipolar");
		this.appMain.bipolarNoiseView();
	}
	
	@FXML private void handleUniformNoise(){
		console.setText("Cambiando Vista: A�adir Ruido Uniforme");
		this.appMain.uniformNoiseView();
	}
}
