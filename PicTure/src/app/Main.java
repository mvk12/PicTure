package app;
	
import java.io.IOException;
import java.util.Optional;

import app.control.AdapEcualizationController;
import app.control.CompareImagesController;
import app.control.EcualizationController;
import app.control.GaussFilterController;
import app.control.GrayScaleController;
import app.control.HistogramController;
import app.control.OutImageController;
import app.control.RootLayoutController;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;


public class Main extends Application {
	
	private Stage firstStage;
	private BorderPane rootLayout;
	
	private RootLayoutController controlRoot;
	
	public static void main(String[] args) {
		launch(args);
	}
	
	public Stage getStage(){ return this.firstStage; }
	
	public void sendToConsole(String msg) { controlRoot.sendMsg(msg); }
	
	@Override public void start(Stage primaryStage) {
		this.firstStage = primaryStage;
		this.firstStage.setTitle("PicTure");
		
		this.firstStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			public void handle(WindowEvent we) {
				Alert close = new Alert(Alert.AlertType.CONFIRMATION);
	  			close.setHeaderText("�Desea Cerrar la Aplicaci�n?");
	  			
	  			Optional<ButtonType> option = close.showAndWait();
	  			String result = option.get().getButtonData().toString();
	  			
	  			if(!result.equals("OK_DONE"))
	  				we.consume();

	          }
	      });

		initRootLayout();
		histogramView();
	}
	
	/*Interface Initial*/
	public void initRootLayout() {
		try{
			/*Load *.fxml */
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/RootLayout.fxml"));
			this.rootLayout = (BorderPane) loader.load();
			
			Scene newScene = new Scene(rootLayout);
			firstStage.setScene(newScene);
			//Principal Windows Controller
			RootLayoutController controller = loader.getController();
			controller.setAppMain(this);
			this.controlRoot = controller;
						
			firstStage.show();
			
		}catch(IOException ioe){
			System.err.println(ioe);
		}
		
	}
	
	/* Histogram View */
	public void histogramView(){
		try{
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/HistogramView.fxml"));
			AnchorPane _anchorPane = (AnchorPane) loader.load();
			
			rootLayout.setCenter(_anchorPane);
			
			//Control
			HistogramController control = loader.getController();
			control.setAppMain(this);
						
		}catch(IOException ioe){
			System.err.println(ioe);
		}
	}
	
	/* Gray Scale View*/
	public void grayScaleView(){
		try{
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/GrayScaleView.fxml"));
			AnchorPane _anchorPane = (AnchorPane) loader.load();
			
			rootLayout.setCenter(_anchorPane);

			GrayScaleController control = loader.getController();
			control.setAppMain(this);
			
		}catch(IOException ioe){
			System.err.println(ioe);
		}
	}
	/* Ecualization View*/
	public void ecualizeView(){
		try{
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/EcualizationView.fxml"));
			AnchorPane _anchorPane = (AnchorPane) loader.load();
			
			rootLayout.setCenter(_anchorPane);
			
			EcualizationController control = loader.getController();
			control.setAppMain(this);
			
		}catch(IOException ioe){
			System.err.println(ioe);
		}
	}
	
	/*Adap Equal View*/
	public void adapEcualizeView(){
		try{
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/AdaptativeEcualizationView.fxml"));
			AnchorPane _anchorPane = (AnchorPane) loader.load();
			
			rootLayout.setCenter(_anchorPane);
			
			AdapEcualizationController control = loader.getController();
			control.setAppMain(this);
			
		}catch(IOException ioe){
			System.err.println(ioe);
		}
	}
	
	/* Compare Images*/
	public void compareImagesView(){
		try{
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/CompareImagesView.fxml"));
			AnchorPane histogramOverview = (AnchorPane) loader.load();
			
			rootLayout.setCenter(histogramOverview);
			
			CompareImagesController control = loader.getController();
			control.setAppMain(this);
			
		}catch(IOException ioe){
			System.err.println(ioe);
		}
	}
	
	/* OutPut Image*/
	public void outImageView(Image image, int offX, int offY, int tol ){
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/OutImageView.fxml"));
			AnchorPane _pane = loader.load();
			
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Result of Comparation");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			
			Scene scene = new Scene(_pane);
			dialogStage.setScene(scene);
			
			OutImageController control = loader.getController();
			control.setAppMain(this);
			control.setdStage(dialogStage);
			control.setLabels(offX, offY, tol);
			control.setImage(image);
			
			dialogStage.showAndWait();
			
		} catch (IOException ioe) {
			System.err.println(ioe);
			sendToConsole(ioe.toString());
		}
	}
	
	public void averageFilterView() {
		// TODO Auto-generated method stub
		
	}
	public void passFiltersView() {
		// TODO Auto-generated method stub
		
	}
	public void gaussFilterView() {
		try{
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/GaussFilterView.fxml"));
			AnchorPane _anchorPane = (AnchorPane) loader.load();
			
			rootLayout.setCenter(_anchorPane);
			
			GaussFilterController control = loader.getController();
			control.setAppMain(this);
			
		}catch(IOException ioe){
			System.err.println(ioe);
		}
	}

	public void ramdomNoiseView() {
		// TODO Auto-generated method stub
		
	}

	public void gaussNoiseView() {
		// TODO Auto-generated method stub
		
	}

	public void bipolarNoiseView() {
		// TODO Auto-generated method stub
		
	}

	public void uniformNoiseView() {
		// TODO Auto-generated method stub
		
	}
}
